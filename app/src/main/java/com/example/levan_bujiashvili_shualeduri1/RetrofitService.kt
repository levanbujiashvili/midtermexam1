package com.example.levan_bujiashvili_shualeduri1

import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {
    @GET("marvel")
    fun getListGrab(): Call<MutableList<ListGrab>>
}