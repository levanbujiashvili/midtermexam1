package com.example.levan_bujiashvili_shualeduri1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_recycle_item.view.*

class MyAdapter(private val context: Context, private val myList: MutableList<ListGrab>): RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            var itemView = LayoutInflater.from(context).inflate(R.layout.layout_recycle_item, parent , false)
            return MyViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return myList.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            Picasso.get().load(myList[position].imageUrl).into(holder.image)
            holder.nameTextView.text = myList[position].name
            holder.realNameTextView.text = myList[position].realName
            holder.bioTextView.text = myList[position].bio
        }

        class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
            var image: ImageView
            var nameTextView: TextView
            var realNameTextView: TextView
            var bioTextView: TextView

            init {
                image = itemView.itemImage
                nameTextView = itemView.nameTextView
                realNameTextView = itemView.realNameTextView
                bioTextView = itemView.bioTextView
            }
        }
}