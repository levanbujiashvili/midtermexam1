package com.example.levan_bujiashvili_shualeduri1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        haventRegisteredText.setOnClickListener() {
            val intent1 = Intent(this, MainActivity::class.java)
            startActivity(intent1)
        }
        loginButton.setOnClickListener() {
            if (loginEditTextEmail.text.toString().isEmpty() || loginEditTextPassword.text.toString().isEmpty()) {
                Toast.makeText(this, "Fill all forms", Toast.LENGTH_SHORT).show()
            } else {
                FirebaseAuth.getInstance().signInWithEmailAndPassword(
                    loginEditTextEmail.text.toString(),
                    loginEditTextPassword.text.toString()
                )
                Toast.makeText(this, "login successful", Toast.LENGTH_SHORT).show()
                val intent5 = Intent(this, RecyclerViewActivity::class.java)
                startActivity(intent5)
            }
        }
    }
}