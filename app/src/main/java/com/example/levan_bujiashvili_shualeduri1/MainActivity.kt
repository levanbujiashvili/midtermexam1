package com.example.levan_bujiashvili_shualeduri1

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.levan_bujiashvili_shualeduri1.Common.retrofitService
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        FirebaseAuth.getInstance()
        init()
    }

    private fun init() {
        alreadyRegisteredText.setOnClickListener() {
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
        }
        //რეგისტრაციის დაჭერა
        regButton.setOnClickListener() {
            if (regEditTextEmail.text.toString().isEmpty() || regEditTextPassword.text.toString().isEmpty()) {
                Toast.makeText(this, "Fill all forms", Toast.LENGTH_SHORT).show()
            } else {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                    regEditTextEmail.text.toString(),
                    regEditTextPassword.text.toString()
                )
                val intent3= Intent(this, Login::class.java)
                startActivity(intent3)
            }
        }
    }
}