package com.example.levan_bujiashvili_shualeduri1

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_recycler_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecyclerViewActivity : AppCompatActivity() {
    private lateinit var adapter:MyAdapter
    lateinit var layoutManager: LinearLayoutManager
    lateinit var dialog: AlertDialog
    lateinit var mService : RetrofitService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)
        mService = Common.retrofitService
        layoutManager = LinearLayoutManager(this)
        myRecyclerView.setHasFixedSize(true)
        myRecyclerView.layoutManager = layoutManager
        dialog = SpotsDialog.Builder().setCancelable(false).setContext(this).build()
        getAllMovieList()
    }
    private fun getAllMovieList() {
        dialog.show()
        mService.getListGrab().enqueue(object : Callback<MutableList<ListGrab>> {
            override fun onFailure(call: Call<MutableList<ListGrab>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<MutableList<ListGrab>>,
                response: Response<MutableList<ListGrab>>
            ) {
                adapter = MyAdapter(baseContext, response.body() as MutableList<ListGrab>)
                adapter.notifyDataSetChanged()
                myRecyclerView.adapter = adapter
                dialog.dismiss()
            }

        })
    }
}
